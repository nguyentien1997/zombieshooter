﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootingGun : MonoBehaviour
{
    //-----THUỘC TÍNH BẮN SÚNG-----//
    float timer;
    Ray shootRay;
    RaycastHit hit;

    //-----THUỘC TÍNH HIỆU ỨNG BẮN SÚNG-----//
    ParticleSystem gunParticles;
    public GameObject bloodEffect;
    LineRenderer gunLine;
    Light gunLight;
    AudioSource gunAudio;
    public static bool isMute;
    public AudioClip muted;
    public AudioClip handGunFire;
    public AudioClip uziFire;
    public AudioClip ak47Fire;
    public AudioClip gatlingGunFire;
    public AudioClip barrettFire;
    float effectsDisplayTime = 0.2f;

    //------THUỘC TÍNH SÚNG------//
    float fireRange;
    float fireRate;
    int damageShooting;
    int amoAk;
    int amoGatlingGun;
    int amoBarret;

    //------THUỘC TÍNH KHÁC------//
    GameObject player;
    GameObject handGun;
    GameObject ak47;
    GameObject gatlingGun;
    GameObject barrett;
    public GameObject[] slotActive;
    int chooseSlot;
    string path = "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/R_hand_container/";

    void Start ()
    {
        isMute = false;
        player = GameObject.FindGameObjectWithTag("Player");
        handGun = player.transform.Find(path + "Handgun").gameObject;
        ak47 = player.transform.Find(path + "Ak47").gameObject;
        gatlingGun = player.transform.Find(path + "GatlingGun").gameObject;
        barrett = player.transform.Find(path + "Barrett").gameObject;
        gunParticles = GetComponent<ParticleSystem>();
        gunLine = GetComponent<LineRenderer>();
        gunLight = GetComponent<Light>();
        gunAudio = GetComponent<AudioSource>();
        effectsDisplayTime = 0.2f;
        chooseSlot = 1;
    }
	
	void Update ()
    {
        timer += Time.deltaTime;
        if (Input.GetMouseButton(0) && timer >= fireRate)
        {
            Shoot();
        }
        if (timer >= fireRate * effectsDisplayTime)
        {
            DisableEffects();
        }
        if (Input.GetKey(KeyCode.Alpha1))
        {
            chooseSlot = 1;
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            chooseSlot = 2;
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            chooseSlot = 3;
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            chooseSlot = 4;
        }
        ChooseWeapon(chooseSlot);
        ActiveSlot(chooseSlot - 1);
    }

    /// <summary>
    /// Hàm bắn đạn
    /// </summary>
    void Shoot()
    {
        timer = 0.0f;
        gunAudio.Play();
        gunLight.enabled = true;
        gunParticles.Play();
        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);
        
        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast(shootRay, out hit, fireRange))
        {
            ZombieControl zombie = hit.collider.GetComponent<ZombieControl>();
            if (zombie != null)
            {
                Instantiate(bloodEffect, hit.point, Quaternion.LookRotation(hit.normal));
                zombie.GetDamage(damageShooting);
            }
            gunLine.SetPosition(1, hit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * fireRange);
        }
    }

    /// <summary>
    /// Hàm bỏ hiệu ứng đường đạn
    /// </summary>
    public void DisableEffects()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }

    /// <summary>
    /// Hàm hiệm ô súng đang sử dụng
    /// </summary>
    /// <param name="slotNum"></param>
    void ActiveSlot (int slotNum)
    {
        for (int i = 0; i < 4; i++)
        {
            if (i == slotNum)
            {
                slotActive[slotNum].SetActive(true);
            }
            else
            {
                slotActive[i].SetActive(false);
            }
        }
    }

    /// <summary>
    /// Hàm chọn súng
    /// </summary>
    /// <param name="choose"></param>
    void ChooseWeapon (int choose)
    {
        switch (choose)
        {
            case 1://=>Chọn Handgun
                {
                    fireRange = 10.0f;
                    fireRate = 0.5f;
                    damageShooting = 10;
                    transform.position = handGun.transform.Find("SpawnBullet").gameObject.transform.position;
                    if (!isMute)
                        gunAudio.clip = handGunFire;
                    else
                        gunAudio.clip = muted;
                    handGun.SetActive(true);
                    ak47.SetActive(false);
                    gatlingGun.SetActive(false);
                    barrett.SetActive(false);
                } break;
            case 2://=>Chọn Ak
                {
                    fireRange = 15.0f;
                    fireRate = 0.25f;
                    damageShooting = 20;
                    transform.position = ak47.transform.Find("SpawnBullet").gameObject.transform.position;
                    if(!isMute)
                        gunAudio.clip = ak47Fire;
                    else
                        gunAudio.clip = muted;
                    handGun.SetActive(false);
                    ak47.SetActive(true);
                    gatlingGun.SetActive(false);
                    barrett.SetActive(false);
                } break;
            case 3://=>Chọn Gatlinggun
                {
                    fireRange = 15.0f;
                    fireRate = 0.1f;
                    damageShooting = 10;
                    transform.position = gatlingGun.transform.Find("SpawnBullet").gameObject.transform.position;
                    if (!isMute)
                        gunAudio.clip = gatlingGunFire;
                    else
                        gunAudio.clip = muted;
                    handGun.SetActive(false);
                    ak47.SetActive(false);
                    gatlingGun.SetActive(true);
                    barrett.SetActive(false);
                } break;
            case 4://=>Chọn Barrett
                {
                    fireRange = 20.0f;
                    fireRate = 1.0f;
                    damageShooting = 100;
                    transform.position = barrett.transform.Find("SpawnBullet").gameObject.transform.position;
                    if (!isMute)
                        gunAudio.clip = barrettFire;
                    else
                        gunAudio.clip = muted;
                    handGun.SetActive(false);
                    ak47.SetActive(false);
                    gatlingGun.SetActive(false);
                    barrett.SetActive(true);
                } break;
        }
    }
}
