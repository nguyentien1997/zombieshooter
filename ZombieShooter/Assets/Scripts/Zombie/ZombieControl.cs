﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieControl : MonoBehaviour
{
    //Khai báo thông số của Zombie
    float moveSpeed;
    int zombieHealth;
    bool isDie;
    int zombieDamage;
    float timeBetweenAttack;
    Animator anim;
    public GameObject player;
    bool playerInRange;
    float timer;
    PlayerControl playerControl;


    // Use this for initialization
    void Start ()
    {
        //Thiết lập thông số
        isDie = false;              //Biến kiểm tra Zombie còn sống hay chết
        zombieHealth = 100;         //Máu của Zombie
        zombieDamage = 10;          //Sát thương của Zombie
        timeBetweenAttack = 1.0f;   //Thời gian giữa mỗi lần đánh của Zombie

        player = GameObject.FindGameObjectWithTag("Player");
        playerControl = player.GetComponent<PlayerControl>();

        //Tăng tốc độ, máu và sát thương theo thời gian
        moveSpeed = 2.0f + (PlayerControl.playTime / 30) * 1f;
        zombieHealth += (PlayerControl.playTime / 30) + 10;
        if(zombieDamage < 70)
            zombieDamage += (PlayerControl.playTime / 30) + 5;
        
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        MoveToPlayer();

        timer += Time.deltaTime;
        if (timer >= timeBetweenAttack && playerInRange && PlayerControl.playerHealth > 0)
        {
            Attack();
        }
    }

    /// <summary>
    /// Hàm di chuyển tới nhân vật
    /// </summary>
    void MoveToPlayer ()
    {
        if(!isDie && !playerInRange && PlayerControl.playerHealth > 0)          //Kiểm tra nếu Zombie còn sống, người chơi còn sống ngoài tầm đánh
        {
            transform.LookAt(player.transform);                                 //Nhìn người chơi
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);  //Di chuyển tới người chơi
        }
    }

    /// <summary>
    /// Hàm gây sát thương vào Zombie
    /// </summary>
    /// <param name="damage"></param>
    public void GetDamage (int damage)
    {
        zombieHealth -= damage;
        if (zombieHealth <= 0)
        {
            isDie = true;
            Die();
        }
    }

    /// <summary>
    /// Hàm Zombie chết
    /// </summary>
    void Die ()
    {
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        gameObject.GetComponent<ZombieControl>().enabled = false;
        anim.SetTrigger("Die");
        Destroy(gameObject, 2f);
        ScoreManager.currentscore++;

        PlayerControl.playerHealth += (PlayerControl.playTime / 30) * 5f;
    }

    /// <summary>
    /// Hàm xử lý khi chạm vào người chơi
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = true;
        }
    }

    /// <summary>
    /// Hàm xử lý khi không chạm vào người chơi nữa
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
            anim.SetBool("Walk", true);
        }
    }


    /// <summary>
    /// Hàm tấn công
    /// </summary>
    void Attack()
    {
        timer = 0f;
        anim.SetTrigger("Attack");
        playerControl.GetDamage(zombieDamage);
    }
}
