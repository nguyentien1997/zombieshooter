﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class PlayerControl : MonoBehaviour
{
    public Text currentTime, highScore, highTime;

    //-----THUỘC TÍNH NHÂN VẬT-----//
    public static float playerHealth;
    public static float realTime;
    public static int playTime;
    public static bool isAlive, isEscape;
    float moveSpeed;
    Vector3 movement;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;
    Animator anim;

    void Start ()
    {
        isAlive = true;
        LoadGame();
        playTime = 0;

        //------Thiết lập thông số nhân vật------//
        moveSpeed = 10.0f;
        playerHealth = 200f;

        floorMask = LayerMask.GetMask("Floor");
        playerRigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }
	
	void Update ()
    {
        HealthBarControl.currentHealth = playerHealth;

        realTime += 10f * Time.timeScale;
        playTime = (int)realTime / 1000;
        
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        currentTime.text = "" + playTime;
        Move(h, v);
        Animating(h, v);
        Turning();
	}

    /// <summary>
    /// Hàm quay nhân vật
    /// </summary>
    void Turning ()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    /// <summary>
    /// Hàm di chuyển nhân vật
    /// </summary>
    /// <param name="h"></param>
    /// <param name="v"></param>
    void Move (float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * moveSpeed * Time.deltaTime;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    /// <summary>
    /// Hàm chuyển Animation Idle thành Animation Run
    /// </summary>
    /// <param name="h"></param>
    /// <param name="v"></param>
    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsRun", walking);
    }

    /// <summary>
    /// Hàm xử lý người chơi chết
    /// </summary>
    void Die()
    {
        anim.SetTrigger("Die");
        //gameObject.GetComponent<PlayerControl>().enabled = false;
        isAlive = false;
        Time.timeScale = 0;
        SaveGame();
    }

    /// <summary>
    /// Hàm người chơi nhận sát thương
    /// </summary>
    /// <param name="damage"></param>
    public void GetDamage (int damage)
    {
        playerHealth -= damage;
        if (playerHealth <= 0)
        {
            Die();
        }
    }

    private SaveManager CreateSaveGameObject()
    {
        SaveManager save = new SaveManager();
        if (SaveManager.highScore < ScoreManager.currentscore)
        {
            SaveManager.highScore = ScoreManager.currentscore;
            SaveManager.highTime = playTime;
        }
        if (SaveManager.highScore == ScoreManager.currentscore)
            if (SaveManager.highTime > playTime)        
                SaveManager.highTime = playTime;            

        return save;
    }

    public void SaveGame()
    {
        SaveManager save = CreateSaveGameObject();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game Saved");
    }

    public void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            SaveManager save = (SaveManager)bf.Deserialize(file);
            file.Close();

            highScore.text = "" + SaveManager.highScore;
            highTime.text = "" + SaveManager.highTime;

            Debug.Log("Game Loaded");
        }
    }
}
