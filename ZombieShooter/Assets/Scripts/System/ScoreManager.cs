﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public static int currentscore;        // The player's score.


    Text currentscoretext;                      // Reference to the Text component.


    void Awake()
    {
        // Set up the reference.
        currentscoretext = GetComponent<Text>();

        // Reset the score.
        currentscore = 0;
    }


    void Update()
    {
        // Set the displayed text to be the word "Score" followed by the score value.
        currentscoretext.text = "" + currentscore;
    }
}
