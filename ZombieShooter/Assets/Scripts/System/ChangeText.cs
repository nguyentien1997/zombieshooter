﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeText : MonoBehaviour
{
    public Text myText = null;
    public int counter = 0;
    //change text on mute button
    public void Change()
    {
        counter++;
        if (counter % 2 == 1)
            myText.text = "Unmute Sound";
        else
            myText.text = "Mute Sound";
    }
}
