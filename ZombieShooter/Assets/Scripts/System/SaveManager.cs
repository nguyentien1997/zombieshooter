﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class SaveManager {

    public static int highScore = 0;
    public static int highTime = 0;
}
