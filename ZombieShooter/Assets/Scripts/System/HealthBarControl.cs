﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarControl : MonoBehaviour
{
    Slider healthBar;
    float maxHealth;
    public static float currentHealth;
    GameObject player;

	void Start ()
    {
        healthBar = GetComponent<Slider>();
        maxHealth = PlayerControl.playerHealth;
        currentHealth = maxHealth;
        healthBar.value = currentHealth / maxHealth;
	}
	
	void Update ()
    {
        healthBar.value = currentHeatlhBar();
	}

    float currentHeatlhBar ()
    {
        return currentHealth / maxHealth;
    }
}
