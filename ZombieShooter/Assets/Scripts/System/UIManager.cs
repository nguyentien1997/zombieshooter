﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    GameObject[] pauseObjects;
    GameObject[] finishObjects;
    int count = 0;
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;

        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");            //gets all objects with tag ShowOnPause
        finishObjects = GameObject.FindGameObjectsWithTag("ShowOnFinish");          //gets all objects with tag ShowOnFinish

        hidePaused();
        hideFinished();

    }

    // Update is called once per frame
    void Update()
    {
        //Khi bấm vào nút Escape sẽ dừng game hoặc tiếp tục game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1 && PlayerControl.isAlive == true)
            {
                Time.timeScale = 0;
                showPaused();
            }
            else if (Time.timeScale == 0 && PlayerControl.isAlive == true)
            {
                Time.timeScale = 1;
                hidePaused();
            }
        }

        //shows finish gameobjects if player is dead and timescale = 0
        if (Time.timeScale == 0 && PlayerControl.isAlive == false)
        {
            showFinished();
        }
    }


    //Reloads the Level
    public void Reload()
    {
        Application.LoadLevel(Application.loadedLevel);
        PlayerControl.realTime = 0f;
    }

    //controls the pausing of the scene
    public void Resume()
    {
        if (Time.timeScale == 0)
            Time.timeScale = 1;
        hidePaused();
    }

    //mute sound
    public void Mute()
    {
        count ++;
        if (count % 2 != 0)
            ShootingGun.isMute = true;
        else
            ShootingGun.isMute = false;
    }

    //shows objects with ShowOnPause tag
    public void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }

    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    //shows objects with ShowOnFinish tag
    public void showFinished()
    {
        foreach (GameObject g in finishObjects)
        {
            g.SetActive(true);
        }
    }

    //hides objects with ShowOnFinish tag
    public void hideFinished()
    {
        foreach (GameObject g in finishObjects)
        {
            g.SetActive(false);
        }
    }

    //loads inputted level
    public void LoadLevel(string level)
    {
        Application.LoadLevel(level);
        PlayerControl.realTime = 0f;
    }
}
