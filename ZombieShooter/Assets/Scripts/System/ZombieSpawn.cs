﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawn : MonoBehaviour
{
    public GameObject zombie;
    GameObject[] spawnPoint;
    float minSpawnTime = 1.0f;
    float maxSpawnTime = 3.0f;
    float spawnTime;
    float lastSpawnTime;
    
	void Start ()
    {
        UpdateSpawnTime();
        spawnPoint = GameObject.FindGameObjectsWithTag("SpawnPoint");
	}
	
	void Update ()
    {
        if (Time.time >= lastSpawnTime + spawnTime)
        {
            Spawn();
        }
	}

    /// <summary>
    /// Hàm cập nhật lại thời gian spawn
    /// </summary>
    void UpdateSpawnTime ()
    {
        lastSpawnTime = Time.time;
        spawnTime = Random.Range(minSpawnTime, maxSpawnTime);
    }

    /// <summary>
    /// Hàm spawn Zombie
    /// </summary>
    void Spawn ()
    {
        int point = Random.Range(0, spawnPoint.Length);
        Instantiate(zombie, spawnPoint[point].transform.position, Quaternion.identity);
        UpdateSpawnTime();
    }
}
