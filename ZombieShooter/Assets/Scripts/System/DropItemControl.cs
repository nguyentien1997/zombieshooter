﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItemControl : MonoBehaviour
{
    public GameObject Medkit;
    public GameObject AmmoBox;
    public static bool isDead;
    public static Vector3 posDrop;

    // Use this for initialization
    void Start ()
    {
        isDead = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isDead == true)
        {
            DropItem();
            isDead = false;
        }
	}

    public void DropItem ()
    {
        int dropChance = Random.Range(0, 100);
        if (dropChance >= 61 && dropChance <= 80)
        {
            Instantiate(Medkit, posDrop, Quaternion.identity);
        }
        if (dropChance >= 81 && dropChance <= 100)
        {
            Instantiate(AmmoBox, posDrop, Quaternion.identity);
        }
    }
}
